package org.apk.CandidateManagement.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = GradResource.class)
public class GradResourceTest {
	
	String idToken = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjQ5MjcxMGE3ZmNkYjE1Mzk2MGNlMDFmNzYwNTIwYTMyYzg0NTVkZmYiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiMTQ0MzE3NTM4NDgzLWIzMGl1YjZldDJqOHIwaXE2M2dibmprYnFmMGw4dHI2LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiMTQ0MzE3NTM4NDgzLWIzMGl1YjZldDJqOHIwaXE2M2dibmprYnFmMGw4dHI2LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTE2MDg0MjU1OTc0MDk4MDEwNjg2IiwiaGQiOiJhY2NvbGl0ZWluZGlhLmNvbSIsImVtYWlsIjoiYXJqdW5wcmFzYWQua2hhbmRrYXJAYWNjb2xpdGVpbmRpYS5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXRfaGFzaCI6IlA0LTg0T3ZvbUc1WWdCdlRxbTJReHciLCJuYW1lIjoiQXJqdW4gS2hhbmRrYXIiLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy1hbkNOM1lhRmp0VS9BQUFBQUFBQUFBSS9BQUFBQUFBQUFBQS9BTVp1dWNrMHBvTy14UjF6WkREWk1TTE9OOW1IRUoxLWR3L3M5Ni1jL3Bob3RvLmpwZyIsImdpdmVuX25hbWUiOiJBcmp1biIsImZhbWlseV9uYW1lIjoiS2hhbmRrYXIiLCJsb2NhbGUiOiJlbiIsImlhdCI6MTU5MTM2NDQ4NywiZXhwIjoxNTkxMzY4MDg3LCJqdGkiOiI0YzMxZGNjYjMyYmY3NDRlNDk3Y2Q1ZDE0MTlmMzE4OWIxMTI0MzFmIn0.BjRjoADvBrxiWHvDzkIT_LAW6jhvUV9IWcFO1fq6a2nvwjsCcYH8VDdz7-98RrApSVuhtA-Adao7gLT_nStnLog-6wQZaVJH2m0X7wJKoGA-gzuVrjwf-Xuj8PR5lf336VIUuJ-aUYIP9d5sBlNMtLIi34avOzgXVu1xFU7gVJfvDdqeuzOiSkjSdG8Vfa6LOE1bpyuQ67DZUUi4_qv8tPev-i4l5WSgXrccQrIwU1dXGCb4QUtZ4k7-b2qngoiwOFwQSBJlxYuFRcxuhbkQD1jpjGhoZ9fFCrZBFH63yy-pfVnAABGGRPLNbYyFPrvNswM3vc_pIuh1PYjPEB-pvw";

	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	private GradResource gradService;
	
	@Test
	void gradsShouldFailWithoutToken() throws Exception {
		this.mockMvc.perform(get("/grad-management/grads"))
			.andExpect(status().isBadRequest());
	}
	
	@Test
	void noGradsContentForBadToken() throws Exception {
		this.mockMvc.perform(get("/grad-management/grads").header("idToken", "abcd"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$").doesNotExist());
	}
	
	@Test
	void gradsByIdShouldFailWithoutToken() throws Exception {
		this.mockMvc.perform(get("/grad-management/grad/1"))
			.andExpect(status().isBadRequest());
	}
	
	@Test
	void noGradsByIdContentForBadToken() throws Exception {
		this.mockMvc.perform(get("/grad-management/grad/1").header("idToken", "abcd"))
			.andExpect(jsonPath("$").doesNotExist());
	}
	
	@Test
	void realTokenShouldWorkGrads() throws Exception {
		this.mockMvc.perform(get("/grad-management/grads")
				.header("idToken", idToken))
				.andExpect(status().isOk());
	}
	
	@Test
	void realTokenShouldWorkGradById() throws Exception {
		this.mockMvc.perform(get("/grad-management/grad/1")
				.header("idToken", idToken))
				.andExpect(status().isOk());
	}
	
	@Test 
	void addGradFailsWithNoToken() throws Exception {
		this.mockMvc.perform(post("/grad-management/add-grad"))
			.andExpect(status().isBadRequest());
	}
	
	@Test
	void addGradFailsWithBadToken() throws Exception {
		this.mockMvc.perform(post("/grad-management/add-grad").header("idToken", "abc"))
			.andExpect(jsonPath("$").doesNotExist());
	}
}
