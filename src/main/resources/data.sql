INSERT IGNORE INTO skills(id, name,createdBy,createdDate) VALUES(1, "Core Java", "auto-entry", '2020-06-05');
INSERT IGNORE INTO skills(id, name,createdBy,createdDate) VALUES(2, "Python", "auto-entry", '2020-06-05');
INSERT IGNORE INTO skills(id, name,createdBy,createdDate) VALUES(3, "Embedded C", "auto-entry", '2020-06-05');
INSERT IGNORE INTO skills(id, name,createdBy,createdDate) VALUES(4, "C++", "auto-entry", '2020-06-05');
INSERT IGNORE INTO skills(id, name,createdBy,createdDate) VALUES(5, "Node JS", "auto-entry", '2020-06-05');
INSERT IGNORE INTO skills(id, name,createdBy,createdDate) VALUES(6, "Spring MVC", "auto-entry", '2020-06-05');
INSERT IGNORE INTO skills(id, name,createdBy,createdDate) VALUES(7, "RDBMS", "auto-entry", '2020-06-05');
INSERT IGNORE INTO skills(id, name,createdBy,createdDate) VALUES(8, "Angular 2", "auto-entry", '2020-06-05');
INSERT IGNORE INTO skills(id, name,createdBy,createdDate) VALUES(9, "jUnit", "auto-entry", '2020-06-05');
INSERT IGNORE INTO skills(id, name,createdBy,createdDate) VALUES(10, "Mockito", "auto-entry", '2020-06-05');
INSERT IGNORE INTO skills(id, name,createdBy,createdDate) VALUES(11, "Swift", "auto-entry", '2020-06-05');

INSERT IGNORE INTO nextId(id) VALUE(1);
