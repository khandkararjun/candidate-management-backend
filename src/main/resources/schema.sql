CREATE TABLE IF NOT EXISTS candidatedb.users (
	email	varchar(255),
	name 	varchar(255) NOT NULL,
	token 	varchar(255) NOT NULL,
	PRIMARY KEY (email)
);

CREATE TABLE IF NOT EXISTS candidatedb.grad (
	id			int				NOT NULL AUTO_INCREMENT,
	createdBy	varchar(255),
	updatedBy	varchar(255),
	createDate	date			NOT NULL,
	updateDate	date			NOT NULL,
	firstName	varchar(30)		NOT NULL,
	middleName	varchar(30)		NOT NULL,
	lastName	varchar(30)		NOT NULL,
	gender		varchar(15)		NOT NULL,
	email		varchar(255)	NOT NULL,
	contact		varchar(20)		NOT NULL,
	addressLn1	varchar(255)	NOT NULL,
	town		varchar(50)		NOT NULL,
	state		varchar(40)		NOT NULL,
	pinCode		varchar(6)		NOT NULL,
	institute	varchar(100)	NOT NULL,
	degree		varchar(15)		NOT NULL,
	branch		varchar(30)		NOT NULL,
	joinDate	date			NOT NULL,
	joinLoc		varchar(50)		NOT NULL,
	feedback	text			NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (createdBy) REFERENCES users(email),
	FOREIGN KEY (updatedBy) REFERENCES users(email)
);

CREATE TABLE IF NOT EXISTS candidatedb.skills (
	id			int				NOT NULL AUTO_INCREMENT,
	name		varchar(50)		NOT NULL,
	createdBy	varchar(255),
	createdDate	date			NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS candidatedb.gradSkillMtM (
	gradId		int,
	skillId		int,
	addDate		date	NOT NULL,
	FOREIGN KEY(gradId) 	REFERENCES grad(id),
	FOREIGN KEY(skillId)	REFERENCES skills(id)
);

CREATE TABLE IF NOT EXISTS candidatedb.liveVersions (
	gradId	int,
	FOREIGN KEY(gradId) REFERENCES grad(id)
);

CREATE TABLE IF NOT EXISTS candidatedb.oldVersions ( 
	gradId	int,
	forId	int,
	FOREIGN KEY(gradId) REFERENCES grad(id),
	FOREIGN KEY(forId) 	REFERENCES liveVersions(gradId)
);

CREATE TABLE IF NOT EXISTS candidatedb.deletedGrads (
	gradId int,
	FOREIGN KEY(gradId) REFERENCES grad(id)
);

CREATE TABLE IF NOT EXISTS nextId (
	id int NOT NULL,
	PRIMARY KEY(id)
);