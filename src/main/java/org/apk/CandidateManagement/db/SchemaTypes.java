package org.apk.CandidateManagement.db;

import java.sql.Types;

public class SchemaTypes {
	
	public static int[] gradSqlTypeArray = new int[] {
			Types.INTEGER,
			Types.VARCHAR,
			Types.VARCHAR,
			Types.DATE,
			Types.DATE,
			Types.VARCHAR,
			Types.VARCHAR,
			Types.VARCHAR,
			Types.VARCHAR,
			Types.VARCHAR,
			Types.VARCHAR,
			Types.VARCHAR,
			Types.VARCHAR,
			Types.VARCHAR,
			Types.VARCHAR,
			Types.VARCHAR,
			Types.VARCHAR,
			Types.VARCHAR,
			Types.DATE,
			Types.VARCHAR,
			Types.LONGNVARCHAR
	};
	
	public static int[] usersSqlTypeArray = new int[] {
			Types.VARCHAR,
			Types.VARCHAR,
			Types.VARCHAR
	};
}
