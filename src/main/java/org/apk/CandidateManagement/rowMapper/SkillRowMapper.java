package org.apk.CandidateManagement.rowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apk.CandidateManagement.model.Skill;
import org.springframework.jdbc.core.RowMapper;

public class SkillRowMapper implements RowMapper<Skill> {

	@Override
	public Skill mapRow(ResultSet rs, int rowNum) throws SQLException {
		Skill skill = new Skill();
		
		skill.setId(rs.getInt("id"));
		skill.setName(rs.getString("name"));
		skill.setCreateDate(rs.getDate("createdDate"));
		skill.setCreator(rs.getString("createdBy"));
		
		return skill;
	}

}
