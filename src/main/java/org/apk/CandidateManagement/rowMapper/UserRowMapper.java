package org.apk.CandidateManagement.rowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apk.CandidateManagement.model.User;
import org.springframework.jdbc.core.RowMapper;

public class UserRowMapper implements RowMapper<User>{

	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		User user = new User();
		
		user.setEmail(rs.getString("email"));
		user.setName(rs.getString("name"));
		user.setToken(rs.getString("token"));
		
		return user;
	}

}
