package org.apk.CandidateManagement.rowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apk.CandidateManagement.model.LocationTrendPoint;
import org.springframework.jdbc.core.RowMapper;

public class LocationTrendPointRowMapper implements RowMapper<LocationTrendPoint> {

	@Override
	public LocationTrendPoint mapRow(ResultSet rs, int rowNum) throws SQLException {
		LocationTrendPoint trendPoint = new LocationTrendPoint();
		
		trendPoint.setTown(rs.getString(1));
		trendPoint.setFrequency(rs.getInt(2));
		
		return trendPoint;
	}
	
}
