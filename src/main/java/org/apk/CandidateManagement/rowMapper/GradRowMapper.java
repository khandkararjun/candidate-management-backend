package org.apk.CandidateManagement.rowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apk.CandidateManagement.model.Grad;
import org.springframework.jdbc.core.RowMapper;

public class GradRowMapper implements RowMapper<Grad> {

	@Override
	public Grad mapRow(ResultSet rs, int rowNum) throws SQLException {
		Grad grad = new Grad();
		
		// Identifiers
		grad.setId(rs.getInt("id"));
		grad.setCreator(rs.getString("createdBy"));
		grad.setLastUpdateBy(rs.getString("updatedBy"));
		// Creation and Last Update date
		grad.setCreateDate(rs.getDate("createDate"));
		grad.setUpdateDate(rs.getDate("updateDate"));
		// Name & Personal Details
		grad.setFname(rs.getString("firstName"));
		grad.setMname(rs.getString("middleName"));
		grad.setLname(rs.getString("lastName"));
		grad.setGender(rs.getString("gender"));
		// Contact Details
		grad.setEmail(rs.getString("email"));
		grad.setContact(rs.getString("contact"));
		// Address
		grad.setAddressLn1(rs.getString("addressLn1"));
		grad.setTown(rs.getString("town"));
		grad.setState(rs.getString("state"));
		grad.setPinCode(rs.getString("pinCode"));
		// Education
		grad.setInstitute(rs.getString("institute"));
		grad.setDegree(rs.getString("degree"));
		grad.setBranch(rs.getString("branch"));
		// AU Joining Details
		grad.setJoinDate(rs.getDate("joinDate")); 
		grad.setJoinLoc(rs.getString("joinLoc"));
		// Feedback
		grad.setFeedback(rs.getString("feedback"));
		
		
		return grad;
	}

}
