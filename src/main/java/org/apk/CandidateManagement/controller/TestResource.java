package org.apk.CandidateManagement.controller;

import java.util.List;

import org.apk.CandidateManagement.model.User;
import org.apk.CandidateManagement.rowMapper.UserRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="test")
public class TestResource {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@GetMapping("/dummyUsers")
	public List<User> dummyUsers() {
		return (List<User>) jdbcTemplate.query("SELECT * FROM users", new UserRowMapper());
	}
	
}
