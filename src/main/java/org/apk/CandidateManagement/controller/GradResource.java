package org.apk.CandidateManagement.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

import org.apk.CandidateManagement.model.ObjectResponseContainer;
import org.apk.CandidateManagement.service.GradService;
import org.apk.CandidateManagement.service.Validator;
import org.apk.CandidateManagement.service.VersionService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apk.CandidateManagement.model.Grad;
import org.apk.CandidateManagement.model.LocationTrendPoint;
import org.apk.CandidateManagement.repository.GradSkillMtMRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="grad-management")
@CrossOrigin(origins="https://localhost:4200")
public class GradResource {

	Logger logger = LogManager.getLogger(GradResource.class);
	
	@Autowired
	GradService gradService;
	@Autowired
	GradSkillMtMRepository gradSkillMtMRepository;
	@Autowired
	VersionService versionService;

	
	// Get all grads
	@GetMapping("/grads")
	public ResponseEntity<ObjectResponseContainer<List<Grad>>> grads(@RequestHeader("idToken") String idToken) throws GeneralSecurityException, IOException {
		ObjectResponseContainer<List<Grad>> resp = new ObjectResponseContainer<List<Grad>>(
				"",
				null);
		
		HttpStatus responseStatus = (!Validator.validateToken(idToken))
				? HttpStatus.UNAUTHORIZED
				: (resp.setAndCheckNull(gradService.grads(versionService.getLiveVersions()))) 
					? HttpStatus.NO_CONTENT
					: HttpStatus.OK;
		
		logger.info("grads: [TOKEN VERIFICATION] "
				+ ((responseStatus == HttpStatus.UNAUTHORIZED)?"FAILURE":"SUCCESS")
				+ " for token "
				+ idToken.substring(0,10));
		
		return ResponseEntity.status(responseStatus).body(resp);
	}

	// Get version history for grad
	@GetMapping("/grad/{id}/history")
	public ResponseEntity<ObjectResponseContainer<List<Grad>>> history(
			@RequestHeader("idToken") String idToken,
			@PathVariable int id) 
			throws GeneralSecurityException, IOException {
		ObjectResponseContainer<List<Grad>> resp = new ObjectResponseContainer<List<Grad>>(
				"",
				null);
		
		HttpStatus responseStatus = (!Validator.validateToken(idToken))
				? HttpStatus.UNAUTHORIZED
				: (resp.setAndCheckNull(gradService.grads(versionService.getOldVersionsFor(id)))) 
					? HttpStatus.NO_CONTENT
					: HttpStatus.OK;
		
		logger.info("history: [TOKEN VERIFICATION] "
				+ ((responseStatus == HttpStatus.UNAUTHORIZED)?"FAILURE":"SUCCESS")
				+ " for token "
				+ idToken.substring(0,10));
		
		return ResponseEntity.status(responseStatus).body(resp);
	}
	
	// Get grad by ID
	@GetMapping("/grad/{id}")
	public ResponseEntity<ObjectResponseContainer<Grad>> grad(@RequestHeader("idToken") String idToken, @PathVariable Integer id) throws GeneralSecurityException, IOException {
		ObjectResponseContainer<Grad> resp = new ObjectResponseContainer<Grad>(
				"",
				null);
		
		HttpStatus responseStatus = (!Validator.validateToken(idToken))
				? HttpStatus.UNAUTHORIZED
				: (resp.setAndCheckNull(gradService.gradById(id)) || !(versionService.getLiveVersions().contains(id)))
					? HttpStatus.NO_CONTENT
					: HttpStatus.OK;
		
		logger.info("grad: [TOKEN_VERIFICATION] "
				+ ((responseStatus == HttpStatus.UNAUTHORIZED)?"FAILURE":"SUCCESS")
				+ " for token "
				+ idToken.substring(0,10));
		
		return ResponseEntity.status(responseStatus).body(resp);
	}

/*
	// Get skills by Grad ID
	// TODO ResponseEntity
	@GetMapping("/grad/{id}/skills")
	public List<Skill> getSkills(@PathVariable Integer id) {
		return gradService.getSkillsByGradId(id);
	}
*/
	// Delete Grad
	// TODO ResponseEntity
	@DeleteMapping("/delete-grad/{id}")
	public ResponseEntity<String> deleteGrad(@PathVariable Integer id) {
		logger.info("Deleting: " + id);
		gradService.newDeleteGrad(id);
		return ResponseEntity.status(200)
				.body("{\"status\":\"ok\"}");
	}
	
	// Add Grad
	@PostMapping("/add-grad")
	public ResponseEntity<ObjectResponseContainer<Integer>> addGrad(@RequestHeader("idToken") String idToken, @RequestBody Grad grad) throws GeneralSecurityException, IOException {
		ObjectResponseContainer<Integer> resp = new ObjectResponseContainer<Integer>(
				"",
				null
				);
	
		HttpStatus responseStatus = (!Validator.validateToken(idToken))
				? HttpStatus.UNAUTHORIZED
				: (gradService.addGrad(grad, true))
					? HttpStatus.OK
					: HttpStatus.INTERNAL_SERVER_ERROR;
					
		
		return ResponseEntity.status(responseStatus).body(resp);
		//return ResponseEntity.status(200).body("{\"status\":\"ok\"}");
	}
	
	// Update Grad
	@PostMapping("/update-grad")
	public ResponseEntity<String> updateGrad(@RequestBody Grad grad) {
		logger.info(grad.toString());
		gradService.updateGrad(grad);
		return ResponseEntity.status(200).body("{\"status\":\"ok\"}");
	}
	
	@GetMapping("/location-trends")
	public ResponseEntity<ObjectResponseContainer<List<LocationTrendPoint>>> locationTrends(@RequestHeader("idToken") String idToken) throws GeneralSecurityException, IOException {
		ObjectResponseContainer<List<LocationTrendPoint>> resp = new ObjectResponseContainer<List<LocationTrendPoint>>(
				"",
				null
				);
		
		HttpStatus responseStatus = (!Validator.validateToken(idToken))
				? HttpStatus.UNAUTHORIZED
				: (resp.setAndCheckNull(gradService.getLocationTrendData()))
					? HttpStatus.NO_CONTENT
					: HttpStatus.OK;
		
		logger.info("locationTrends: [TOKEN_VERIFICATION] "
				+ ((responseStatus == HttpStatus.UNAUTHORIZED)?"FAILURE":"SUCCESS")
				+ " for token "
				+ idToken.substring(0,10));
		
		return ResponseEntity.status(responseStatus).body(resp);
	}
	
	// Add skill for grad
	// TODO remove
	/*
	@PostMapping("/grad/{id}/add-skill")
	public void addGradSkill(@PathVariable int id, @RequestBody GradSkill skill) {
		gradSkillMtMRepository.addSkillForGrad(id, skill.getSkillId(), skill.getCreatorEmail());
	}*/
}
