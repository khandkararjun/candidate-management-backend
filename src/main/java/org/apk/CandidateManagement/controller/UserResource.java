package org.apk.CandidateManagement.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.apk.CandidateManagement.model.User;
import org.apk.CandidateManagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="login")
public class UserResource {

	@Autowired
	UserService userService;

	@PostMapping("")
	@CrossOrigin(origins="https://localhost:4200")
	public User addUser(@RequestHeader("idToken") String idToken, @RequestBody User user) throws GeneralSecurityException, IOException {
		
		if(userService.addUser(idToken, user)) {
			return user;
		}
		// TODO Return created user
		return null;
	}
}
