package org.apk.CandidateManagement.controller;

import java.util.List;

import org.apk.CandidateManagement.model.ObjectResponseContainer;
import org.apk.CandidateManagement.model.Skill;
import org.apk.CandidateManagement.repository.SkillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="skill-management")
@CrossOrigin(origins="https://localhost:4200")
public class SkillResource {

	@Autowired
	SkillRepository skillRepository;

	// Unused
	/*
	@PostMapping("/add-skill")
	public ResponseEntity<String> addSkill(@RequestBody Skill skill) {
		skillRepository.addSkill(skill);
		return ResponseEntity.status(200).body("OK");
	}*/
	
	@GetMapping("/skills")
	public ResponseEntity<ObjectResponseContainer<List<Skill>>> getSkills() {
		ObjectResponseContainer<List<Skill>> resp = new ObjectResponseContainer<List<Skill>>(
				"",
				skillRepository.getSkills()
				);
		
		return ResponseEntity.status(200).body(resp);
	}
}
