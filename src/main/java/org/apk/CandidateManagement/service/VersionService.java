package org.apk.CandidateManagement.service;

import java.util.List;

import org.apk.CandidateManagement.repository.VersionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VersionService implements VersionServiceInterface {
	
	@Autowired
	VersionRepository versionRepository;

	@Override
	public void addLiveVersion(Integer id) {
		versionRepository.addLiveVersion(id);
	}

	@Override
	public void deleteLiveVersion(Integer id) {
		versionRepository.deleteLiveVersion(id);
		
	}
	
	@Override
	public void deleteOldVersions(Integer id) {
		versionRepository.deleteOldVersions(id);
	}
	
	@Override
	public void addToDeletedList(Integer id) {
		versionRepository.addToDeletedList(id);
	}
	
	@Override
	public void addOldVersionFor(Integer id, Integer oldVersionFor) {
		versionRepository.addOldVersion(id, oldVersionFor);
	}

	@Override
	public List<Integer> getLiveVersions() {
		return versionRepository.getLiveVersionIds();
	}

	@Override
	public List<Integer> getOldVersionsFor(Integer id) {
		return versionRepository.getOldVersionsFor(id);
	}

}
