package org.apk.CandidateManagement.service;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apk.CandidateManagement.model.User;
import org.apk.CandidateManagement.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
	
	Logger logger = LogManager.getLogger(UserService.class);

	@Autowired
	UserRepository userRepository;
	
	public boolean addUser(String idToken, User user) throws GeneralSecurityException, IOException {
		
		if(!Validator.validateToken(idToken)) {
			logger.info("INVALID: idToken:" + idToken.substring(0,10) + "... for user:" + user.getEmail());
			return false;
		}
		
		if(userRepository.getUser(user) == null)
			userRepository.addUser(user);
		else
			userRepository.updateUserToken(user);
		
		logger.info("Validated idToken:" + idToken.substring(0,10) + "... for user:" + user.getEmail());
		return true;
	}
	// TODO create class for this
	// Validate for all API calls
	// upon failure return error model
	/*
	public boolean validateToken(String token) throws GeneralSecurityException, IOException {
		HttpTransport transport = GoogleNetHttpTransport.newTrustedTransport();
		JsonFactory jsonFactory = new JacksonFactory();
		GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(transport, jsonFactory)
				.setAudience(Collections.singletonList("144317538483-b30iub6et2j8r0iq63gbnjkbqf0l8tr6.apps.googleusercontent.com"))
				.build();
		
		return ((verifier.verify(token)) != null);
	}*/
}
