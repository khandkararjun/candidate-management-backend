package org.apk.CandidateManagement.service;

import java.util.List;

public interface VersionServiceInterface {
	public void addLiveVersion(Integer id);
	public void deleteLiveVersion(Integer id);
	public void addOldVersionFor(Integer id, Integer oldVersionFor);
	public List<Integer> getLiveVersions();
	public List<Integer> getOldVersionsFor(Integer id);
	void addToDeletedList(Integer id);
	void deleteOldVersions(Integer id);
}
