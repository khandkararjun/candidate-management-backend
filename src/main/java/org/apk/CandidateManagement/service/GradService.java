package org.apk.CandidateManagement.service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apk.CandidateManagement.model.Grad;
import org.apk.CandidateManagement.model.LocationTrendPoint;
import org.apk.CandidateManagement.model.Skill;
import org.apk.CandidateManagement.repository.AutoIdRepository;
import org.apk.CandidateManagement.repository.GradRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//TODO logging

@Service
public class GradService {
	
	Logger logger = LogManager.getLogger(GradService.class);

	@Autowired
	GradRepository gradRepository;
	@Autowired
	VersionService versionService;
	@Autowired
	AutoIdRepository autoIdRepository;
	
	public List<Grad> grads(List<Integer> ids) {
		final List<Grad> grads = new LinkedList<Grad>();
		ids.forEach(id -> {
			grads.add(this.gradById(id));
		});
		return grads;
	}
	
	public Grad gradById(int id) {
		Grad grad;
		
		if((grad = gradRepository.getGradById(id)) != null) {
			logger.info("Setting skills:");
			grad.setSkills(gradRepository.getSkillsByGradId(id));
			logger.info(grad.getSkills().toString());
		}
		
		return grad;
	}
	
	public List<Skill> getSkillsByGradId(int id) {
		return gradRepository.getSkillsByGradId(id);
	}

	// old
	public void deleteGrad(int id) {
		versionService.deleteOldVersions(id);
		versionService.deleteLiveVersion(id);
		gradRepository.deleteGrad(id);
	}
	
	// new
	public void newDeleteGrad(int id) {
		List<Integer> oldVersions = versionService.getOldVersionsFor(id);
		versionService.deleteOldVersions(id);
		versionService.deleteLiveVersion(id);
		// First, delete all old versions of the grad
		oldVersions.forEach(oldVersionId -> {
			gradRepository.deleteGrad(oldVersionId);
		});
		
		// Finally, add the ID to the deleted grads list
		versionService.addToDeletedList(id);
	}

	public boolean addGrad(Grad grad, boolean isNewEntry) {
		// Get the next Grad ID from the autoID repository
		Integer id = autoIdRepository.getNextId();
		// Set Grad ID as such
		grad.setId(id);
		
		if(gradRepository.addGrad(grad) >= 0) {
			// Update the autoID repository
			autoIdRepository.updateId();
			// Add entry to table of live version
			if(isNewEntry)
				versionService.addLiveVersion(id);
			
			return true;
		}
	
		return false;
	}
	
	public void updateGrad(Grad grad) {
		// Get the current Grad ID
		Integer currentId = grad.getId();
		
		// Duplicate the Grad record to create an old version
		// First, get the current record
		Grad currentVersion = gradRepository.getGradById(currentId);
		currentVersion.setSkills(gradRepository.getSkillsByGradId(currentId));
		// Then, get the new ID for the old version entry
		Integer oldVersionId = autoIdRepository.getNextId();
		currentVersion.setId(oldVersionId);
		// Create the old version
		this.addGrad(currentVersion, false);
		
		// Update the version repository with the newly created old version:
		versionService.addOldVersionFor(oldVersionId, currentId);
		
		// Finally, update grad to create the new version
		gradRepository.updateGrad(grad);
	}
	
	public List<LocationTrendPoint> getLocationTrendData() {
		return gradRepository.getLocationTrendData();
	}
}
