package org.apk.CandidateManagement.repository;

import java.util.Date;
import java.util.List;

import org.apk.CandidateManagement.model.Grad;
import org.apk.CandidateManagement.model.LocationTrendPoint;
import org.apk.CandidateManagement.model.Skill;
import org.apk.CandidateManagement.rowMapper.GradRowMapper;
import org.apk.CandidateManagement.rowMapper.LocationTrendPointRowMapper;
import org.apk.CandidateManagement.rowMapper.SkillRowMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apk.CandidateManagement.db.SchemaTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class GradRepository {
	
	public static boolean NEW_ENTRY = true;
	public static boolean OLD_VERSION = false;
	
	Logger logger = LogManager.getLogger(GradRepository.class);
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	@Autowired
	SkillRepository skillRepository;
	
	public List<Grad> getGrads() {
		try {
			return (List<Grad>) jdbcTemplate.query(
					"SELECT * FROM grad",
					new GradRowMapper()
					);
		} catch(EmptyResultDataAccessException e) {
			return null;
		}
	}
	
	public Grad getGradById(int id) {
		try {
			return jdbcTemplate.queryForObject(
					"SELECT * FROM grad WHERE id=?",
					new Object[] {id},
					new GradRowMapper()
					);
		} catch(EmptyResultDataAccessException e) {
			return null;
		}
	}

	public List<Skill> getSkillsByGradId(int id) {
		return jdbcTemplate.query(
				"SELECT * " +
				"FROM skills " +
				"INNER JOIN gradSkillMtM " + 
				"ON skills.id=gradSkillMtM.skillId " +
				"WHERE gradSkillMtM.gradId=?",
				new Object[] {id},
				new SkillRowMapper()
				);
	}

	public void deleteGrad(int id) {
		jdbcTemplate.update(
				"DELETE FROM gradSkillMtM WHERE gradId=?",
				new Object[] {id}
		);
		jdbcTemplate.update(
				"DELETE FROM grad WHERE id=?",
				new Object[] {id}
		);
	}

	public int addGrad(Grad grad) {
		int affected;
		
		try {
			affected = jdbcTemplate.update(
				"INSERT INTO grad VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
				new Object[] {
					grad.getId(),
					grad.getCreator(),
					grad.getLastUpdateBy(),
					new Date(),
					new Date(),
					grad.getFname(),
					grad.getMname(),
					grad.getLname(),
					grad.getGender(),
					grad.getEmail(),
					grad.getContact(),
					grad.getAddressLn1(),
					grad.getTown(),
					grad.getState(),
					grad.getPinCode(),
					grad.getInstitute(),
					grad.getDegree(),
					grad.getBranch(),
					grad.getJoinDate(),
					grad.getJoinLoc(),
					grad.getFeedback()
				},
				SchemaTypes.gradSqlTypeArray
				);
			skillRepository.addSkillsForGrad(grad.getSkills(), grad.getId());
			return affected;
		} catch (DataAccessException e) {
			return -1;
		}
	}
	
	public void updateGrad(Grad grad) {
		jdbcTemplate.update(
				"UPDATE grad SET"
				+ " updatedBy = ?,"
				+ " updateDate = ?,"
				+ " firstName = ?,"
				+ " middleName = ?,"
				+ " lastName = ?,"
				+ " gender = ?,"
				+ " email = ?,"
				+ " contact = ?,"
				+ " addressLn1 = ?,"
				+ " town = ?,"
				+ " state = ?,"
				+ " pinCode = ?,"
				+ " institute = ?,"
				+ " degree = ?,"
				+ " branch = ?,"
				+ " joinDate = ?,"
				+ " joinLoc = ? "
				+ "WHERE id=?",
				new Object[] {
					grad.getLastUpdateBy(),
					new Date(),
					grad.getFname(),
					grad.getMname(),
					grad.getLname(),
					grad.getGender(),
					grad.getEmail(),
					grad.getContact(),
					grad.getAddressLn1(),
					grad.getTown(),
					grad.getState(),
					grad.getPinCode(),
					grad.getInstitute(),
					grad.getDegree(),
					grad.getBranch(),
					grad.getJoinDate(),
					grad.getJoinLoc(),
					grad.getId()
				});
		
		// A somewhat wonky way of updating skills, but it does the trick
		skillRepository.deleteSkillsForGrad(grad.getId());
		skillRepository.addSkillsForGrad(grad.getSkills(), grad.getId());
	}
	
	public List<LocationTrendPoint> getLocationTrendData() {
		return jdbcTemplate.query(
				"SELECT town, COUNT(*) FROM grad GROUP BY town",
				new LocationTrendPointRowMapper());
	}
}
