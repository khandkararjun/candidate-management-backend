package org.apk.CandidateManagement.repository;

import org.apk.CandidateManagement.model.User;
import org.apk.CandidateManagement.rowMapper.UserRowMapper;
import org.apk.CandidateManagement.db.SchemaTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepository {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	//Get users
	public User getUser(User user) {
		try {
			return jdbcTemplate.queryForObject(
					"SELECT * FROM users WHERE email=?",
					new Object[] {user.getEmail()},
					new UserRowMapper()
			);
		} catch(EmptyResultDataAccessException e) {
			return null;
		}
	}
	
	public void addUser(User user) {
		jdbcTemplate.update(
				"INSERT INTO users VALUES(?,?,?)",
				new Object[] {
						user.getEmail(),
						user.getName(),
						user.getToken()},
				SchemaTypes.usersSqlTypeArray
		);
	}
	
	public void updateUserToken(User user) {
		jdbcTemplate.update(
				"UPDATE users SET token=? WHERE email=?",
				new Object[] {
						user.getToken(),
						user.getEmail()
				}
		);
	}
}
