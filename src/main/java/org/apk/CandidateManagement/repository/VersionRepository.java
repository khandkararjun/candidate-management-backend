package org.apk.CandidateManagement.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class VersionRepository {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public void addLiveVersion(Integer gradId) {
		jdbcTemplate.update(
				"INSERT INTO liveVersions VALUES(?)",
				new Object[] {
					gradId
				});
	}
	
	public void deleteLiveVersion(Integer gradId) {
		jdbcTemplate.update(
				"DELETE FROM liveVersions WHERE gradId=?",
				new Object[] {
					gradId
				});
	}
	
	public void deleteOldVersions(Integer id) {
		jdbcTemplate.update(
				"DELETE FROM oldVersions WHERE forId=?",
				new Object[] {
					id
				});
	}
	
	public void addToDeletedList(Integer id) {
		jdbcTemplate.update(
				"INSERT INTO deletedGrads VALUES(?)",
				new Object[] {
					id
				});
	}
	
	public void addOldVersion(Integer oldVersionId, Integer oldVersionFor) {
		jdbcTemplate.update(
				"INSERT INTO oldVersions VALUES(?,?)",
				new Object[] {
					oldVersionId,
					oldVersionFor
				});
	}
	
	public List<Integer> getLiveVersionIds() {
		return jdbcTemplate.query(
				"SELECT * FROM liveVersions",
				new RowMapper<Integer> () {
					public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
						return rs.getInt(1);
					}
				});
	}
	
	public List<Integer> getOldVersionsFor(Integer id) {
		return jdbcTemplate.query(
				"SELECT gradId FROM oldVersions WHERE forId=?",
				new Object[] {id},
				new RowMapper<Integer> () {
					public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
						return rs.getInt(1);
					}
				});
	}
}
