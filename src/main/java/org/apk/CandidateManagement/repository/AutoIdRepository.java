package org.apk.CandidateManagement.repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class AutoIdRepository {
	
	Logger logger = LogManager.getLogger(AutoIdRepository.class);
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	// Get the next ID to be assigned
	public Integer getNextId() {
		Integer nextId = null;
		nextId = jdbcTemplate.queryForObject(
				"SELECT * FROM nextId",
				new RowMapper<Integer>() {
					public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
						return rs.getInt("id");
					}
				});
		if(nextId == null)
			logger.info("nextId was null");
		return nextId;
	}

	// Increment the ID by 1 
	public void updateId() {
		jdbcTemplate.update("UPDATE nextId SET id=id+1");
	}
}
