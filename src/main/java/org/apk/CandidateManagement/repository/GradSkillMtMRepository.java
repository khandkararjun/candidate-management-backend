package org.apk.CandidateManagement.repository;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class GradSkillMtMRepository {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public void addSkillForGrad(int gradId, int skillId, String user) {
		jdbcTemplate.update(
				"INSERT INTO gradSkillMtM VALUES(?,?,?)",
				new Object[] {
						gradId,
						skillId,
						new Date()
				}
		);
	}
}
