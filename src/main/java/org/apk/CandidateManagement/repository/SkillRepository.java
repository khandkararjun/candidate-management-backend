package org.apk.CandidateManagement.repository;

import java.util.Date;
import java.util.List;

import org.apk.CandidateManagement.model.Skill;
import org.apk.CandidateManagement.rowMapper.SkillRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class SkillRepository {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public void addSkill(Skill skill) {
		jdbcTemplate.update(
				"INSERT INTO skills VALUES(?,?,?,?)",
				new Object[] {
					skill.getId(),
					skill.getName(),
					skill.getCreator(),
					skill.getCreateDate()
				}
		);
	}
	
	public List<Skill> getSkills() {
		return jdbcTemplate.query(
				"SELECT * FROM skills",
				new SkillRowMapper()
				);
	}

	public void deleteSkillsForGrad(int gradId) {
		jdbcTemplate.update(
				"DELETE FROM gradSkillMtM WHERE gradId=?",
				new Object[] {
					gradId
				});
	}
	
	public void addSkillsForGrad(List<Skill> skills, int gradId) {
		for(Skill skill: skills) {
			jdbcTemplate.update(
					"INSERT INTO gradSkillMtM VALUES(?,?,?)",
					new Object[] {
						gradId,
						skill.getId(),
						new Date()
					});
		}
	}
}
