package org.apk.CandidateManagement.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class ObjectResponseContainer<T> 
{
	String status;
	T responseObject;
	
	public boolean setAndCheckNull(T obj) {
		this.responseObject = obj;
		return (obj == null);
	}
}
