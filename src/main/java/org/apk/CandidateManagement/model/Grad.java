package org.apk.CandidateManagement.model;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class Grad {
	@Override
	public String toString() {
		return "Grad [id=" + id + ", creator=" + creator + ", lastUpdateBy=" + lastUpdateBy + ", createDate="
				+ createDate + ", updateDate=" + updateDate + ", fname=" + fname + ", mname=" + mname + ", lname="
				+ lname + ", gender=" + gender + ", email=" + email + ", contact=" + contact + ", addressLn1="
				+ addressLn1 + ", town=" + town + ", state=" + state + ", pinCode=" + pinCode + ", institute="
				+ institute + ", degree=" + degree + ", branch=" + branch + ", joinDate=" + joinDate + ", joinLoc="
				+ joinLoc + ", feedback=" + feedback + ", skills=" + skills + "]";
	}
	Integer id;
	String creator;
	String lastUpdateBy;
	Date createDate;
	Date updateDate;
	String fname;
	String mname;
	String lname;
	String gender;
	String email;
	String contact;
	String addressLn1;
	String town;
	String state;
	String pinCode;
	String institute;
	String degree;
	String branch;
	Date joinDate;
	String joinLoc;
	String feedback;
	List<Skill> skills;
}
