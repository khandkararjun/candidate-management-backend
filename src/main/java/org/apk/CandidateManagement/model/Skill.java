package org.apk.CandidateManagement.model;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Skill {
	@Override
	public String toString() {
		return "Skill [creator=" + creator + ", createDate=" + createDate + ", id=" + id + ", name=" + name + "]";
	}
	String creator;
	Date createDate;
	Integer id;
	String name;
}
